# BACnet to MQTT Automatic Mapping

## Use Docker/ARMv7 folder for WAGO controllers
## Use Docker/AMD64 folder for Edge PC or other Debian Linux devices

## Project Details

- [ ] Written in Python
- [ ] Encapsulated in container for use in Docker

## Name
BACnet to MQTT Automatic Mapping

## Description
This docker container will detect all BACnet devices on the network and map the Present Value and Units properties of all Analog Input, Analog Output, Analog Value, Binary Input, Binary Output, and Binary Value objects. Each object and its properties is added to a library that is updated and transmitted every five seconds. Each object properties also has an MQTT topic created associated with it. Publishing a value to that topic will send the change to the BACnet object.

## Load the docker tar file
```docker load < bac2mqtt64.tar```
```docker load < bac2mqtt_local.tar```

## Docker run command
```docker run -e Broker_Address="192.168.1.130" -e Broker_Port=1883 --network=host -it bac2mqtt64:latest```
```docker run -e Broker_Address="192.168.1.130" -e Broker_Port=1883 --network=host -it bac2mqtt_local:latest```

## MQTT structure
The topic ```DeviceList``` contains the master list of all devices, as well as the objects within the device, and present value.
![Program](screenshot.png)

This specific version of the Docker image contains support for Unencrypted MQTT communication ONLY

## Reboot
Publishing a value of ```true``` to the MQTT topic ```reboot``` will restart the BACnet process and rescan the network for devices.
This feature does not work on ARMv7 devices.

## Support
This program is for demonstration only and does not include support.  May contain bugs.  Use at your own risk.

## Authors and acknowledgment
Joe Abdelmalak and Adam Reeve
MAY 14 2024

## License
Copyright (c) 2024, Adam Reeve, Joe Abdelmalak, WAGO

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
